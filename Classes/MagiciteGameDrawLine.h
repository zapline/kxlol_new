#ifndef __MAGICITE_GAME_DRAW_LINE__
#define __MAGICITE_GAME_DRAW_LINE__

#include "cocos2d.h"

class MagiciteGameDrawLine
{
public:
    MagiciteGameDrawLine();
    ~MagiciteGameDrawLine();

    static MagiciteGameDrawLine* create(
        const cocos2d::Vec2& pointA,
        const cocos2d::Vec2& pointB,
        float width,
        const cocos2d::Color4F& color);

    void setColor(const cocos2d::Color4F& color);
    cocos2d::Color4F getColor() const;

    void setWidth(float width);
    float getWidth() const;

    void setPointA(const cocos2d::Vec2& point);
    cocos2d::Vec2 getPointA() const;

    void setPointB(const cocos2d::Vec2& point);
    cocos2d::Vec2 getPointB() const;

    bool operator== (const MagiciteGameDrawLine& line) const;

private:
    cocos2d::Color4F        _color;
    cocos2d::Vec2           _from;
    cocos2d::Vec2           _to;
    float                   _width;
};

#endif //__MAGICITE_GAME_DRAW_LINE__